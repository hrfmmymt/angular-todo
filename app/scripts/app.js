'use strict';

angular.module('angularTodoApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize'
])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

var mainCtrl = function($scope, $http) {
  $scope.tasks = [
  {"body":"do this 1", "done":false},
  {"body":"do this 2", "done":false},
  {"body":"do this 3", "done":true},
  {"body":"do this 4", "done":false}
  ];
  $scope.addNew = function() {
    $scope.tasks.push({"body":$scope.newTasksBody,"done":false});
    $scope.newTasksBody = '';
  }
  $scope.getDoneCount = function() {
    var count = 0;
    angular.forEach($scope.tasks, function(task) {
      count += task.done ? 1 : 0;
    });
    return count;
  }
  $scope.deleteDone = function() {
    var oldTasks = $scope.tasks;
    $scope.tasks = [];
    angular.forEach(oldTasks, function(task) {
      if (!task.done) $scope.tasks.push(task);
    });
  }
}
